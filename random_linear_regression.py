import random

import pandas as pd

df = pd.read_csv('house_pricing.csv')

matrix = df.values

X = matrix[:,0:1]
y = matrix[:,1]


best_b0 = 56
best_b1 = 100
best_cost_value = 99999999999999

for i in range(10000):
    b0 = random.randint(-10000, 10000)
    b1 = random.randint(-10000, 10000)
    y_pred = b0 + X[:, 0] * b1

    cost = (abs(y_pred - y) / y).mean()
    if best_cost_value > cost:
        best_cost_value = cost
        best_b0 = b0
        best_b1 = b1

print(best_cost_value)
print(best_b0, best_b1)

y_pred = best_b0 + X[:, 0] * best_b1
mse = ((y_pred - y) ** 2).mean()
print(mse)
