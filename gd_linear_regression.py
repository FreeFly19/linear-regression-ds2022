import pandas as pd
import numpy as np

df = pd.read_csv('house_pricing.csv')

matrix = df.values

X = matrix[:,0:1]
y = matrix[:,1]

ones = np.array([
    [1],
    [1],
    [1],
    [1]
])

X = np.concatenate([ones, X], axis=1)
b = np.array([7441, 688])

lr = 0.000001
for i in range(100000):
    error_d_b = (X @ b - y) * 2 @ X
    b = b - lr * error_d_b

print(X @ b)


