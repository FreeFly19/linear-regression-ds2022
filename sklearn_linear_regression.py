import pandas as pd
from sklearn.linear_model import LinearRegression

df = pd.read_csv('house_pricing.csv')

matrix = df.values

X = matrix[:,0:1]
y = matrix[:,1]

model = LinearRegression()
model.fit(X, y)
y_pred = model.predict(X)

mse = ((y_pred - y) ** 2).mean()
print(mse)
